package net.therap.notestasks.config;

/**
 * @author tanmoy.das
 * @since 5/2/20
 */
public class Constants {

    public static final String CURRENT_USER = "currentUserCommand";

    public static final String REGISTER_USER_COMMAND = "registerUserCommand";

    public static final String LOGIN_USER_COMMAND = "loginUserCommand";

    public static final String INDEX_PAGE = "index";

    public static final String CONTEXT_PATH = "/notestasks";
    public static final String USER_TXT = "user";
    public static final String IS_USER_CONNECTED_TXT = "isUserConnected";
    public static final String IS_REQUEST_SENT_TXT = "isRequestSent";
    public static final String IS_REQUEST_RECEIVED_TXT = "isRequestReceived";
    public static final String IS_MYSELF_TXT = "isMyself";
    public static final String REDIRECT_PROFILE = "redirect:/profile";
    public static final String DASHBOARD_PAGE = "dashboard";
    public static final String REDIRECT_MESSAGES = "redirect:/messages";
    public static final String REDIRECT_NOTES = "redirect:/notes";
    public static final String REDIRECT_TASKS = "redirect:/tasks";
    public static final String REDIRECT_DASHBOARD = "redirect:/dashboard";
}
