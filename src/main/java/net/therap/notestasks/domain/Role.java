package net.therap.notestasks.domain;

/**
 * @author tanmoy.das
 * @since 4/22/20
 */
public enum Role {
    BASIC_USER,
    ADMIN
}
