package net.therap.notestasks.domain;

/**
 * @author tanmoy.das
 * @since 5/5/20
 */
public enum AccessLevel {
    READ,
    WRITE,
    SHARE,
    DELETE
}
